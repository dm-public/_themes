

$(document).ready(function() {

window.cookieconsent.initialise({
container: document.getElementById("cookie_container"),
  "palette": {
    "popup": {
      "background": "#eb6c44",
      "text": "#ffffff"
    },
    "button": {
      "background": "#f5d948"
    }
  },
  "theme": "classic",
  "content": {
    "message": "Um unsere Webseite für Sie optimal zu gestalten und fortlaufend verbessern zu können, verwenden wir Cookies. Durch die weitere Nutzung der Webseite stimmen Sie der Verwendung von Cookies zu.\nWeitere Informationen zu Cookies erhalten Sie in unserer Datenschutzerklärung",
    "dismiss": "OK",
    "link": "Weitere Informationen",
    "href": "/impressum/"
  }
});
    
    });