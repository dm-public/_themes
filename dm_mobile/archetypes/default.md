---
markup: "mmark"
title: "Artikel Titel"
draft: false
date: {{ .Date }}
publishdate: {{ .Date }}
lastmod: {{ .Date }}
image: "photos/beispiel_pilz.jpg"
comments: false
description: Custom Artikel Beschreibung
main_header: Custom H1 Überschrift
categories:
  - kategorie1
tags:
  - tag1
keywords: Artikelkeyword1, Artikelkeyword2
---

# Erste Überschrift